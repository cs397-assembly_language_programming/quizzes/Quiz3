;***************************************
;	Question2.asm
;	Date: 1 May 2017
;	Quiz 3, question 2: 
;			Write a complete assembly language program to read a vector (1-dim array)			
;			A = (A 1 ,�,A N ), and display Sum(k=1 to N) (Ak)^2 . 
;			Procedures must be used to organize the code.
;
;	Author: Jose Madureira
;***************************************

Include Irvine32.inc
.data
	;data storage
	arrA dword 100 DUP(?)
	aSize dword ?

	;messages
	msgEV byte "Please enter values: ",0
	msgA byte 09h,"value 1: ",0
	msgHM byte "How many values?",0dh,0ah,09h," >>number: ",0
	msgRS byte "Running sum: ",0

.code
Main PROC
	mov edx,offset msgHM	
	call WriteString		;"How many numbers will be entered? "
	call ReadDec			;get size of array
	call Crlf
	mov aSize,eax

	mov edx, offset msgEV
	call WriteString		;"Please enter values: "
	call Crlf

	xor ecx,ecx				;initialize counter
getvals:
	;get array A value
	mov edx, offset msgA
	call WriteString		;"value n: ", n increased after
	inc byte ptr[edx+7]
		
	call ReadInt
	mov [arrA+ecx*4],eax	;store value in arrA

	inc ecx
	cmp ecx, [aSize]
jb getvals					;loop until all numbers are entered

	mov esi, offset arrA
	call SumArr				;sum values


	call Crlf
	call Crlf

	exit
Main ENDP


;************************************************
SumArr PROC uses eax ebx ecx edx
;* Purpose: Sum(k=1 to N) (Ak)^2				*
;*----------------------------------------------*
;* IN : esi, arrA								*
;* OUT: none									*
;************************************************
	mov edx,offset msgRS
	call Crlf
	call WriteString		;"Running sum: "

	xor ecx,ecx				;initialize counter
	xor ebx,ebx				;initialize running sum holder
addN:
	mov eax,[esi+ecx*4]		;ebx= arrA[n]
	mul eax					;ebx^2
	add ebx,eax				;keep running sum

	;sisplay running sum
	call Crlf
	mov al, 09h
	call WriteChar			;display tab characeter

	mov eax,ebx
	call WriteDec			;display current running count

	inc ecx
	cmp ecx,aSize
jb addN						;loop until all elements are added

ret
SumArr ENDP

END Main
