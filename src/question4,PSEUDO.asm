;***************************************
;	Question4.asm
;
;	Date: 1 May 2017
;	Quiz3, question 4: 
;		Write procedure MinMax that receives a variable number of integers and returns the
;		minimum and the maximum integer in the list. The min and max values are stored in
;		memory locations pointed to by addresses passed to MinMax. The Main procedure requests
;		the input registers from the user. Positive and negative integer values are valid. The user
;		entering zero terminates the input integer sequence. The min and max values returned are
;		displayed by the Main procedure.
;
;	Author: Jose Madureira
;***************************************

;************************************************
MinMax PROC uses ecx edx
;* Purpose: calculates the min and max 			*
;*----------------------------------------------*
;* IN : esi, offset to array with numbers		*
;* OUT: eax, minimum							*
;*		ebx, maximum							*
;************************************************
	xor ecx,ecx			;index counter

;set initial value
	mov eax,0
	mov ebx,0

;get values
rpt:
	mov edx,[esi+ecx*4]		;get one value
	cmp eax,0				;if user enters zero then they are done
	je finish

	cmp eax,edx
	jb setMin				;set new minimum if new number is smaller
	
	cmp ebx,edx
	ja setMax				;set new maximum if new number is bigger

jmp rpt		;loop until all numbers are comapred

setmin:
	mov eax,edx
	jmp rpt
setmax:
	mov ebx,edx
	jmp rpt

finish:
	

ret
MinMax ENDP


END Main
