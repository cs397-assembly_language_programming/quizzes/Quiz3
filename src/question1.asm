;***************************************
;	Question1.asm
;	Date: 1 May 2017
;	Quiz: 
;		Write a complete assembly language program to read two vectors (1-dim arrays)
;		A and B and display the resulting vector C, which is the sum of A and B. 
;		Procedures must be used to organize the code.	
;
;	Author: Jose Madureira
;***************************************

Include Irvine32.inc
.data
	;data storage
	arrA dword 100 DUP(?)
	arrB dword 100 DUP(?)
	arrC dword 100 DUP(?)
	aSize dword ?

	;messages
	msgEV byte "Please enter values: ",0
	msgA byte 09h,"array A: ",0
	msgB byte 09h,"array B: ",0
	msgHM byte "How many values in each array?",0dh,0ah,09h," >>number: ",0
	msgCvals byte "Array C values: ",0

.code
Main PROC
	mov edx,offset msgHM	
	call WriteString		;"How many numbers will be entered? "
	call ReadDec			;get size of array
	mov aSize,eax

	mov edx, offset msgEV
	call WriteString		;"Please enter values: "
	call Crlf
	call Crlf

	xor ecx,ecx				;initialize counter
getvals:
	;get array A value
	mov edx, offset msgA
	call WriteString		;"array A: "
	call ReadInt
	mov [arrA+ecx*4],eax	;store value in first array (arrA)

	;get array B value
	mov edx, offset msgB
	call WriteString		;"array B: "
	call ReadInt
	mov[arrB+ecx*4],eax	;store value in first array (arrA)
	call Crlf

	inc ecx
	cmp ecx, aSize
jl getvals					;loop until all numbers are entered in both arrays
	
	;pass location of arrays
	mov esi,offset arrA
	mov edi,offset arrB
	mov ebx,offset arrC
	call SumArr				;receives 3 array addresses: esi,edi,ebx

	mov esi,offset arrC
	call DispArrSum			;displays values in arrC


	call Crlf
	call Crlf

	exit
Main ENDP


;************************************************
SumArr PROC uses eax ebx ecx edx
;* Purpose: arrC[n] = arrA[n] + arrB[n]			*
;*----------------------------------------------*
;* IN : esi, arrA								*
;*		edi, arrB								*
;*		ebx, arrC								*
;* OUT: none									*
;************************************************
	mov ecx,0				;index counter
addN:
	mov eax,[esi+ecx*4]		;eax= arrA
	add eax,[edi+ecx*4]		;eax= arrA + arrB 
	mov [ebx+ecx*4],eax		;store sum in arrC

	inc ecx
	cmp ecx,aSize
jb addN					;loop until all elements are added

ret
SumArr ENDP

;************************************************
DispArrSum PROC uses eax ecx edx
;* Purpose: displays values in arrC				*
;*----------------------------------------------*
;* IN : esi, arrC								*
;* OUT: none									*
;************************************************
	mov edx,offset msgCvals
	call WriteString		;"arrC values: "
	call Crlf
	
	mov ecx,0				;index counter
showArr:
	mov eax,[esi+ecx*4]
	call WriteInt			;display arrC[N] value
	call Crlf

	inc ecx
	cmp ecx,aSize
jb showArr				;loop until all arrC are displayed

ret
DispArrSum ENDP


END Main
