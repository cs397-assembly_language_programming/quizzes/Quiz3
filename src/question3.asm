;***************************************
;	Question3.asm
;
;	Date: 1 May 2017
;	Quiz3, question 3: 
;		Write a complete assembly language program to read two matrices (2-dim arrays) A and B
;		and display the resulting matrix C, which is the sum of A and B. Procedures must be used to
;		organize the code.	
;	Author: Jose Madureira
;***************************************

Include Irvine32.inc
.data
	;data storage
	arrA dword 100 DUP(?)
	arrB dword 100 DUP(?)
	arrC dword 100 DUP(?)
	aSize dword ?
	cols dword ?

	;messages
	msgEV byte "Please enter values: ",0
	msgA byte 09h,"matrix A: ",0
	msgB byte 09h,"matrix B: ",0
	msgHM byte "How many values in each matrix?",0dh,0ah,09h," >>number: ",0
	msgCvals byte "Matrix C values: ",0
	msgDimC byte "How many values per row?",0

.code
Main PROC
	mov edx,offset msgHM	
	call WriteString		;"How many values in each matrix?"	
	call ReadDec			;get size of matrix array
	mov aSize,eax

	mov edx,offset msgDimC		;"How many values per row?"
	call WriteString
	call ReadDec
	mov cols,eax

	mov edx, offset msgEV
	call Crlf
	call WriteString		;"Please enter values: "
	call Crlf
	call Crlf

	xor ecx,ecx				;initialize counter
getvals:
	;get array A value
	mov edx, offset msgA
	call WriteString		;"matrix A: "
	call ReadInt
	mov [arrA+ecx*4],eax	;store value in first array (arrA)

	;get array B value
	mov edx, offset msgB
	call WriteString		;"matrix B: "
	call ReadInt
	mov[arrB+ecx*4],eax	;store value in first array (arrA)
	call Crlf

	inc ecx
	cmp ecx, aSize
jl getvals					;loop until all values entered
	
	;pass location of arrays
	mov esi,offset arrA
	mov edi,offset arrB
	mov ebx,offset arrC
	call SumArr				;receives 3 array addresses: esi,edi,ebx

	call Crlf

	;display Matrix of sums
	mov esi,offset arrC
	call DispArrSum			;receives 1 array address: esi


	call Crlf
	call Crlf

	exit
Main ENDP


;************************************************
SumArr PROC uses eax ebx ecx edx
;* Purpose: arrC[n] = arrA[n] + arrB[n]			*
;*----------------------------------------------*
;* IN : esi, arrA								*
;*		edi, arrB								*
;*		ebx, arrC								*
;* OUT: none									*
;************************************************
	mov ecx,0				;index counter
addN:
	mov eax,[esi+ecx*4]		;eax= arrA
	add eax,[edi+ecx*4]		;eax= arrA + arrB 
	mov [ebx+ecx*4],eax		;store sum in arrC

	inc ecx
	cmp ecx,aSize
jb addN					;loop until all elements are added

ret
SumArr ENDP

;************************************************
DispArrSum PROC uses eax ebx ecx edx
;* Purpose: displays values in arrC				*
;*----------------------------------------------*
;* IN : esi, arrC								*
;* OUT: none									*
;************************************************
	mov edx,offset msgCvals
	call WriteString		;"Matrix c values: "
	call Crlf

	mov ebx,0				;row counter	
	mov ecx,0				;col counter

	;show a single row
showRow:
	;exit if only partial row is to be shown
	cmp ebx,aSize	
	je past

	mov eax,[esi+ecx*4]
	call WriteInt			;display arrC[N] value

	mov al,7ch				;pipe '|'
	call WriteChar

	inc ecx
	cmp ecx,cols
jb showRow					;loop until all columns are displayed
	
	;display new row
	call Crlf
	add ebx,ecx
	cmp ebx,aSize			;if all displays then quit
	jb showRow				;else display more

past:

ret
DispArrSum ENDP


END Main
