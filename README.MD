Date: 20170522


		1. Write a complete assembly language program to read two vectors (1-dim arrays) A and B and display the resulting vector C, which is the sum of A and B. Procedures must be used to organize the code.

		2. Write a complete assembly language program to read a vector (1-dim array) A = (A 1 ,�,A N ), and display Sum(k=1 to N) (A k ) 2 . Procedures must be used to organize the code.

		3. Write a complete assembly language program to read two matrices (2-dim arrays) A and B and display the resulting matrix C, which is the sum of A and B. Procedures must be used to organize the code.

		4. Write procedure MinMax that receives a variable number of integers and returns the minimum and the maximum integer in the list. The min and max values are stored in memory locations pointed to by addresses passed to MinMax. The Main procedure requests the input registers from the user. Positive and negative integer values are valid. The user entering zero terminates the input integer sequence. The min and max values returned are displayed by the Main procedure.